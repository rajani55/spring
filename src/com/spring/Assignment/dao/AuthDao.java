package com.spring.Assignment.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.Assignment.entity.LoginDto;
import com.spring.Assignment.entity.RegisterDto;



@Repository
public class AuthDao {

	@Autowired
	private SessionFactory sessionfactory ;
	
	public void saveRegisterDetails(RegisterDto registerDto)
	{
	Session session = sessionfactory.openSession();
	try
	{
		Transaction transaction = session.beginTransaction();
		session.save(registerDto);
		transaction.commit();
		
	}
	catch(Exception e) {}
	finally
	{
		session.close();
	}
	}
	
	public RegisterDto getRegisterDataByEmailAndPwd(LoginDto loginDTO) {
		Session session = sessionfactory.openSession();
		String hql="from RegisterDto where email=:e and password=:p";
		 Query query = session.createQuery(hql);
		query.setParameter("e", loginDTO.getEmail());
		query.setParameter("p", loginDTO.getPassword());
		RegisterDto register = (RegisterDto) query.uniqueResult();
		return  register;
	}
	
	
	
}
