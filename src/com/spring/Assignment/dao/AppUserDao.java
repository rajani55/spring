package com.spring.Assignment.dao;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.common.util.impl.Log_.logger;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.Assignment.entity.AppUserDetailsDto;


@Repository
public class AppUserDao {
	
	private static final org.jboss.logging.Logger logger = LoggerFactory.logger(AppUserDao.class);
	@Autowired
	private SessionFactory sessionfactory ;
	public AppUserDao() {
		System.out.println(this.getClass().getSimpleName()+"  object created");
	}
	public void saveuserData(AppUserDetailsDto appUserDetailsDto)
	{
	Session session = sessionfactory.openSession();
	try
	{
		Transaction transaction = session.beginTransaction();
		session.save(appUserDetailsDto);
		transaction.commit();
		
	}
	catch(Exception e) {}
	finally
	{
		session.close();
	
	}

}
	public List<AppUserDetailsDto> getAppdetailsByUserId(Long id) {
		Session session = sessionfactory.openSession();
		String hql =" from AppUserDetailsDto where registerDto.id=:i";
		Query query = session.createQuery(hql);
		query.setParameter("i", id);
		List<AppUserDetailsDto> list=query.list();
		return list;
	}
	
	public List<AppUserDetailsDto> list() {
		Session session = sessionfactory.openSession();
		List<AppUserDetailsDto> List = session.createQuery("from AppUserDetailsDto").list();
		for(AppUserDetailsDto q : List){
			logger.info("Person List::"+q);
		}
		return List;
	}
	public AppUserDetailsDto getPersonById(int id) {
		Session session = sessionfactory.openSession();	
		AppUserDetailsDto q = (AppUserDetailsDto) session.load(AppUserDetailsDto.class, new Integer(id));
		logger.info("Person loaded successfully, Person details="+q);
		return q;
	}
	}


}
