package com.spring.Assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.Assignment.dao.AuthDao;
import com.spring.Assignment.entity.LoginDto;
import com.spring.Assignment.entity.RegisterDto;


@Service
public class AuthService {

	@Autowired
	private AuthDao authdao ;
		public void saveRegisterDetails(RegisterDto registerDto)
	{
		authdao.saveRegisterDetails(registerDto);
	}
		public RegisterDto getRegisterDataByEmailAndPwd(LoginDto loginDTO) {
			return authdao.getRegisterDataByEmailAndPwd(loginDTO);
		}


}