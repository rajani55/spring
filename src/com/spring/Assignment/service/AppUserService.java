package com.spring.Assignment.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.Assignment.dao.AppUserDao;
import com.spring.Assignment.entity.AppUserDetailsDto;



@Service
public class AppUserService {
	
	@Autowired
	private AppUserDao appUserDao ;
	public AppUserService() {
		System.out.println(this.getClass().getSimpleName()+"  object created");
	}
	public void saveUserData(AppUserDetailsDto appUserDetailsDto)
	{
		appUserDao.saveuserData(appUserDetailsDto);
	}
	public List<AppUserDetailsDto> getAppdetailsByUserId(Long id) {
		return appUserDao.getAppdetailsByUserId(id);
	}
	public List<AppUserDetailsDto> list() {
		return this.appUserDao.list();
	}
	
	public AppUserDetailsDto getPersonById(int id) {
		return this.appUserDao.getPersonById(id);
	}



	

}
