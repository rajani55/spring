package com.spring.Assignment.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="App_User")
public class AppUserDetailsDto implements Serializable{
	@Id
	@GenericGenerator(name="userauto",strategy="increment")
	@GeneratedValue(generator="userauto")
	@Column(name="id")
	private Long id;
	@Column(name="appName")
	private String appName;
	@Column(name="userName")
	private String userName;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	public AppUserDetailsDto() {
		// TODO Auto-generated constructor stub
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "r_id")
	private RegisterDto registerDto;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RegisterDto getRegisterDto() {
		return registerDto;
	}

	public void setRegisterDto(RegisterDto registerDto) {
		this.registerDto = registerDto;
	}

	@Override
	public String toString() {
		return "AppUserDetailsDto [id=" + id + ", appName=" + appName + ", userName=" + userName + ", email=" + email
				+ ", password=" + password + ", registerDto=" + registerDto + "]";
	}

	
}
