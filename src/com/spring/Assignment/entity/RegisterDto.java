package com.spring.Assignment.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="Register_details_spring_Assignment")
public class RegisterDto implements Serializable {

	@Id
	@GenericGenerator(name="userauto",strategy="increment")
	@GeneratedValue(generator="userauto")
	@Column(name="id")
	private Long id;
	@Column(name="city")
	private String city;
	@Column(name="country")
	private String country;
	@Column(name="email")
	private String email;
	@Column(name="contactnumber")
	private String contactnumber;
	@Column(name="password")
	private String password;
	public RegisterDto() {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Register [id=" + id + ", city=" + city + ", country=" + country + ", email=" + email
				+ ", contactnumber=" + contactnumber + ", password=" + password + "]";
	}
	
	

}