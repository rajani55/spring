package com.spring.Assignment.controller;

import java.util.List;

//import javax.enterprise.inject.Model;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.Assignment.entity.AppUserDetailsDto;
import com.spring.Assignment.entity.RegisterDto;
import com.spring.Assignment.service.AppUserService;

@Controller
@RequestMapping(value="/")
public class AppUserController {
	
	@Autowired
	private AppUserService appuserservice;
	public AppUserController() {
		System.out.println(this.getClass().getSimpleName()+"  object created");
	}
	/*@RequestMapping(value="saveuserdata")
	public ModelAndView saveUserData(AppUserDetailsDto appUserDetailsDto)
	{
	System.out.println(appUserDetailsDto);
	appuserservice.saveUserData(appUserDetailsDto);
	return new ModelAndView("home.jsp");
	
	}*/
	
	@RequestMapping(value = "saveUserData")
	public ModelAndView saveUserData(AppUserDetailsDto appUserDetails,HttpServletRequest request) {
		System.out.println(appUserDetails);
		RegisterDto register = (RegisterDto) request.getSession().getAttribute("registerDto");
		appUserDetails.setRegisterDto(register);
		System.out.println(appUserDetails);
		appuserservice.saveUserData(appUserDetails);
		List<AppUserDetailsDto> list = appuserservice.getAppdetailsByUserId(register.getId());
		return new ModelAndView("homepage.jsp","list",list);
	}
	
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public ModelAndView listDetails(HttpServletRequest request,Model model) {
		RegisterDto register = (RegisterDto) request.getSession().getAttribute("registerDto");
		(model).addAttribute("RegisterDto", new RegisterDto());
		model.addAttribute("listPersons", this.appuserservice.list());
		List<AppUserDetailsDto> list = appuserservice.getAppdetailsByUserId(register.getId());
		return new ModelAndView("homepage.jsp" ,"list",list);
	}

	@RequestMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") int id, Model model){
        model.addAttribute("person", this.appuserservice.getPersonById(id));
        model.addAttribute("listPersons", this.appuserservice.list());
        List<AppUserDetailsDto> list = appuserservice.getAppdetailsByUserId(register.getId());
        return new ModelAndView("homepage.jsp" ,"list",list);;
    }

}
