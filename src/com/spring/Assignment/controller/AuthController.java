package com.spring.Assignment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.Assignment.entity.AppUserDetailsDto;
import com.spring.Assignment.entity.LoginDto;
import com.spring.Assignment.entity.RegisterDto;
import com.spring.Assignment.service.AppUserService;
import com.spring.Assignment.service.AuthService;



@Controller
@RequestMapping("/")
public class AuthController {

	@Autowired
	private AuthService authService;
	
	@Autowired
	private AppUserService appUserService;


	//@PostMapping(name = "*/saveRegisterDetails")
	@RequestMapping(value="saveRegisterDetails")
	public ModelAndView saveRegisterDetails(RegisterDto  registerDto) {
		authService.saveRegisterDetails(registerDto);
		return new ModelAndView("login.jsp","msg","Registraion successfull please login!");
	}
	
	//@PostMapping(name="*/login")
	/*@RequestMapping(value="login")
	public ModelAndView login(LoginDto loginDTO) {
		RegisterDto register = authService.getRegisterDataByEmailAndPwd(loginDTO);
		if(register != null) {
			
			return new ModelAndView("homepage.jsp");
		}
		return new ModelAndView("login.jsp","msg","Invalid credentials!!");
	}*/
	
	@RequestMapping("/login")
	public ModelAndView login(LoginDto loginDTO,HttpServletRequest request) {
		RegisterDto register = authService.getRegisterDataByEmailAndPwd(loginDTO);
		if(register != null) {
			HttpSession session = request.getSession();
			session.setAttribute("registerDto", register);
			List<AppUserDetailsDto> list = appUserService.getAppdetailsByUserId(register.getId());
			list.forEach(a->{
				System.out.println(a);
			});
			return new ModelAndView("homepage.jsp","list",list);
		}
		return new ModelAndView("login.jsp","msg","Invalid credentials!!");
	}
	
	/*@RequestMapping(value="/edit/{RegisterDto}",method=RequestMethod.get)
	public void edit(@PathVariable("RegisterDto"))
	{
		
	}*/

    /*@RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") int id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
        model.addAttribute("listPersons", this.personService.listPersons());
        return "person";
    }*/
	//@RequestMapping("/edit/{id}")
	
	

}