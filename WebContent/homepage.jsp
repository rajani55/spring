<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a href="userdetails.jsp">+create</a>
<table border="1">
<thead>
<tr>
<th>id</th>
<th>Application name</th>
<th>userName</th>
<th>email</th>
<th>password</th>
<th>settings</th>
</tr>
</thead>
<tbody>
 		<c:forEach items="${list}" var="app">
 			<tr>
 				<td>${app.id}</td>
 				<td>${app.appName}</td>
 				<td>${app.userName}</td>
 				<td>${app.email}</td>
 				<td>${app.password}</td>
 				<td><a href="<c:url value='/edit/${app.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/view/${app.id}' />" >view</a></td>
		</tr>
 
</c:forEach>
	</tbody>

</table>
</body>
</html>